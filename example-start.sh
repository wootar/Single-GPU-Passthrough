#!/bin/bash
# Helpful to read output when debugging
set -x

# Stop SDDM
systemctl stop sddm.service
# Start OpenSSH Server
systemctl start openssh.service
## Uncomment the following line if you use GDM
#killall gdm-x-session

# Unbind VTconsoles
echo 0 > /sys/class/vtconsole/vtcon0/bind
echo 0 > /sys/class/vtconsole/vtcon1/bind

# Unbind EFI-Framebuffer
echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/unbind

# Avoid a Race condition by waiting 3 seconds. This can be calibrated to be shorter or longer if required for your system
sleep 3

# Unbind the GPU from display driver
virsh nodedev-detach pci_0000_01_00_0
virsh nodedev-detach pci_0000_01_00_1

# Unbind HDA (Audio device)
virsh nodedev-detach pci_0000_00_1b_0

# Unbind USB Controllers
virsh nodedev-detach pci_0000_00_1d_0
virsh nodedev-detach pci_0000_00_1a_0
virsh nodedev-detach pci_0000_05_00_0

modprobe vfio
modprobe vfio_pci
modprobe vfio_iommu_type1
