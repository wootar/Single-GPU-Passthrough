#!/bin/bash
set -x

# Unbind VFIO
modprobe -r vfio
modprobe -r vfio_pci
modprobe -r vfio_iommu_type1

# Re-Bind GPU to Nvidia Driver
virsh nodedev-reattach pci_0000_01_00_0
virsh nodedev-reattach pci_0000_01_00_1

# Rebind HDA (Audio device)
virsh nodedev-reattach pci_0000_00_1b_0

# Rebind USB Controllers
virsh nodedev-reattach pci_0000_00_1d_0
virsh nodedev-reattach pci_0000_00_1a_0
virsh nodedev-reattach pci_0000_05_00_0

# Reload modules
modprobe nvidia
modprobe nvidia_modeset
modprobe nvidia_uvm
modprobe nvidia_drm
modprobe xhci_hcd
modprobe snd_hda_intel
modprobe ehci-pci

# Rebind VT consoles
echo 1 > /sys/class/vtconsole/vtcon0/bind
# Some machines might have more than 1 virtual console. Add a line for each corresponding VTConsole
#echo 1 > /sys/class/vtconsole/vtcon1/bind

nvidia-xconfig --query-gpu-info > /dev/null 2>&1
echo "efi-framebuffer.0" > /sys/bus/platform/drivers/efi-framebuffer/bind

# Restart SDDM
systemctl start sddm.service
# Stop OpenSSH Server
systemctl stop openssh.service
